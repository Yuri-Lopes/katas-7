// forEach()
// map()
// some()
// find()
// findIndex()
// every()
// filter()

//kata 1 --newForEach

Array.prototype.newForEach = function (foeach) {
    for (let i = 0; i < this.length; i++) {
        foeach(this[i]);
    }
}
// function logArrayElements(element, index, array) {
//   console.log("a[" + index + "] = " + element);
// }
//NewforEACH
// function testNewForEach(element, index) {
//     console.log("a[" + index + "] = " + element);
// }

// kata 2 --newMap

Array.prototype.newMap = function (nwmap) {
    let result = [];
    for (let i = 0; i < this.length; i++) {
        arr.push(nwmap(this[i]));
    }
    return result;
}
//MAP
// let numbers = [1, 4, 9];
// let roots = numbers.map(Math.sqrt);
// console.log(roots);
//NEW MAP
// let numbers2 = [1, 4, 9];
// let roots2 = numbers2.newMap(Math.sqrt);
// console.log(roots2);




// kata 3 --newSome

Array.prototype.newSome = function (news) {
    for (let i = 0; i < this.length; i++) {
        if (news(this[i]) === true)
            return true;
    }
    return false;
}
// function isBiggerThan10(element, index, array) {
//   return element > 10;
// }
// console.log([12, 5, 8, 1, 4].newSome(isBiggerThan10));


//kata 4 --newFind

Array.prototype.newFind = function (nfinded) {
    for (let i = 0; i < this.length; i++) {
        if (nfinded(this[i]) === true)
            return this[i];
    }
}
// console.log(test.find(isBiggerThan10));
// }
//New find
// console.log(test.newFind(isBiggerThan10));


// kata 5 -- newFindIndex

Array.prototype.newFindIndex = function (nfi) {
    for (let i = 0; i < this.length; i++) {
        if (nfi(this[i]) === true)
            return i;
    }
    return -1;
}
//find
// console.log(test.findIndex(isBiggerThan10));
//
//New find
// console.log(test.newFindIndex(isBiggerThan10));




// kata 6 -- newEvery

Array.prototype.newEvery = function (newY) {
    for (let i = 0; i < this.length; i++) {
        if (newY(this[i]) !== true)
            return false;
    }
    return true;
}

// function isBigEnough(element, index, array) {
//   return element >= 10;
// }
// console.log([12, 5, 8, 130, 44].newEvery(isBigEnough))
// false
// console.log([12, 54, 18, 130, 44].newEvery(isBigEnough))
// true


//kata 7 -- newFilter

Array.prototype.newFilter = function (nfilter) {
    let arr = [];
    for (let i = 0; i < this.length; i++) {
        if (nfilter(this[i]) === true)
            arr.push(this[i]);

    }
    return arr;
}
//find
// console.log(test.filter(isBiggerThan10));
//
//New find
// console.log(test.newFilter(isBiggerThan10));

